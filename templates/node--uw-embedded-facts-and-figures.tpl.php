<?php

/**
 * @file
 * Default theme implementation to display a facts and figures node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php

  $nid = $node->nid;
  if ($nid):
    $fact_point_present = TRUE;
    $view_result = views_get_view_result('embedded_facts_and_figures', 'call_to_action_page', $nid);

    $counter = 0;

    foreach ($view_result[0]->field_field_fact_figure[0]['rendered']['entity']['paragraphs_item'] as $vr):

      $show_bubbles = FALSE;
      if (isset($vr['field_eff_hf_show_bubbles'][0]['#markup'])):
        $show_bubbles = $vr['field_eff_hf_show_bubbles'][0]['#markup'];
        if ($show_bubbles == "" || $show_bubbles == "Yes"):
          $show_bubbles = TRUE;
        else:
          $show_bubbles = FALSE;
        endif;

      else:
        $show_bubbles = TRUE;

      endif;

      // TODO: Check with UR about colour choices for bubble colour.
      $background_colour = 'grey';
      $data = [];
      $data[$counter]['view_id'] = $counter;

      $default_colour = $vr['field_eff_hf_default_colour'][0]['#markup'];
      $start = strpos($default_colour, '(');
      $end = strpos($default_colour, ')', $start + 1);
      $length = $end - $start;
      $default_colour_class = strtolower(substr($default_colour, $start + 1, $length - 1));
      // Shim to rename Health to legacy AHS naming.
      if ($default_colour_class == 'health') {
        $default_colour_class = 'ahs';
      }

      $text_align = $vr['field_eff_hf_text_align'][0]['#markup'];

      while ($fact_point_present):
        $fact_point = $vr['field_eff_hf_fact_points'][$counter];

        if (isset($fact_point['entity']['field_collection_item'])):
          foreach ($fact_point['entity']['field_collection_item'] as $fp):
            if (isset($fp['field_eff_hf_icon']['#items'][0]['uri'])):
              $data[$counter]['icon'] = file_create_url($fp['field_eff_hf_icon']['#items'][0]['uri']);
              $data[$counter]['alt'] = $fp['field_eff_hf_icon']['#items'][0]['alt'];
            else:
              $data[$counter]['icon'] = NULL;
              $data[$counter]['alt'] = NULL;
            endif;

            if (isset($fp['field_eff_hf_type_of_graphic'][0]) && isset($fp['field_eff_hf_fact']['#items'][0]['value'])) {

              // Set the type of infographic.
              $infographic_type = $fp['field_eff_hf_type_of_graphic']['#items'][0]['value'];
              $data[$counter]['infographic_type'] = $infographic_type;

              // Get the value, convert to a percentage.
              $infographic_value = $fp['field_eff_hf_fact']['#items'][0]['value'];
              if (strrpos($infographic_value, '%') !== FALSE) {
                $data[$counter]['infographic_precent'] = rtrim($infographic_value, '%');
              }
              elseif (strrpos($infographic_value, ':') !== FALSE) {
                $radio_parts = explode(':', $infographic_value);
                $data[$counter]['infographic_precent'] = round((int) $radio_parts[0] / (int) $radio_parts[1] * 100);
              }
              elseif (strrpos($infographic_value, '/') !== FALSE) {
                $fraction_parts = explode('/', $infographic_value);
                $data[$counter]['infographic_precent'] = round((int) $fraction_parts[0] / (int) $fraction_parts[1] * 100);
              }
              elseif (is_numeric($infographic_value) || (preg_match('/^\d{1,3}(,\d{3})*(\.\d+)?$/', $infographic_value))) {
                $data[$counter]['infographic_precent'] = $infographic_value;
              }
              if (($infographic_type == 'number') && isset($fp['field_infographic_fact_suffix'][0])) {
                $data[$counter]['infographic_suffix'] = $fp['field_infographic_fact_suffix']['#items'][0]['value'];
              }
              // These need to be passed as hex codes for the circle chart script.
              $faculty_colours = array(
                'ahs' => '#005963',
                'arts' => '#D93F00',
                'engineering' => '#57058B',
                'environment' => '#607000',
                'math' => '#C60078',
                'science' => '#0033BE',
                'uwaterloo' => '#FFD54F'
              );
              $data[$counter]['infographic_colour'] = $faculty_colours[$default_colour_class];
            }

            if (isset($fp['field_eff_hf_caption'][0]['#markup'])):
              $data[$counter]['caption'] = $fp['field_eff_hf_caption'][0]['#markup'];
            else:
              $data[$counter]['caption'] = '';
            endif;

            $text_counter = 0;
            while (array_key_exists($text_counter, $fp['field_eff_hf_text'])):
              foreach ($fp['field_eff_hf_text'][$text_counter]['entity']['field_collection_item'] as $text_items):
                $data[$counter]['text'][$text_counter]['text_text'] = isset($text_items['field_eff_hf_text_text'][0]['#markup']) ? $text_items['field_eff_hf_text_text'][0]['#markup'] : NULL;
                if (isset($text_items['field_eff_hf_text_type_of_text'][0]['#markup'])):
                  $data[$counter]['text'][$text_counter]['text_type'] = $text_items['field_eff_hf_text_type_of_text'][0]['#markup'];
                else:
                  $data[$counter]['text'][$text_counter]['text_type'] = "";
                endif;
              endforeach;
              $text_counter++;
            endwhile;
          endforeach;
        endif;

        $counter++;
        if (isset($vr['field_eff_hf_fact_points'])):
          $fact_point_present = array_key_exists($counter, $vr['field_eff_hf_fact_points']);
        endif;

      endwhile;

    endforeach;

    $data_usecarousel = 'Yes';

    $data_numberpercarousel = 3;

    $html = '';

    $html .= '<div id="ff-' . $nid . '"';
    if ($show_bubbles):
      $html .= ' class="ff-with-bubbles"';
    endif;
    $html .= '>';

    if ($data_usecarousel == "Yes"):
      $html .= '<div class="highlighted-fact-wrapper-' . $nid . ' owl-theme ' . strtolower($background_colour) . '" data-highlighted-carousel data-nid="' . $nid . '" data-num-per="' . $data_numberpercarousel . '" data-use-carousel="' . $data_usecarousel . '">';
    else:
      $html .= '<div class="highlighted-fact-wrapper-' . $nid . ' ' . strtolower($background_colour) . '" data-nid="' . $nid . '">';
    endif;

    foreach ($data as $data_item):
      if (!empty($data_item['icon'])):
        $html .= '<div class="highlighted-fact text-' . strtolower($text_align) . ' ' . $default_colour_class . '">';
      else:
        $html .= '<div class="highlighted-fact text-' . strtolower($text_align) . ' ' . $default_colour_class . ' no-icon">';
      endif;

      if (!empty($data_item['icon'])):
        $attr = array();
        $attr['src'] = $data_item['icon'];
        if (!empty($data_item['alt'])):
          $attr['alt'] = $data_item['alt'];
        endif;

        $html .= '<img' . drupal_attributes($attr) . '>';
      endif;

      if (!empty($data_item['infographic_type'])):
        $html .= '<div class="highlighted-fact-infographic infographic-' . $data_item['infographic_type'] . '" data-infographic-type="' . $data_item['infographic_type'] . '" data-percent="' . $data_item['infographic_precent'] . '" data-foregroundColor="' . $data_item['infographic_colour'] . '">';
        if ($data_item['infographic_type'] == 'number') {
          $html .= '<span class="number"><span class="timer">' . $data_item['infographic_precent'] . '</span>';
          if (!empty($data_item['infographic_suffix'])) {
           $html .= $data_item['infographic_suffix'];
          }
          $html .= '</span>';
        }
        $html .= '</div>';
      endif;

      if (isset($data_item['text']) && ($data_item['text'] !== '')):
        foreach ($data_item['text'] as $di):
          $html .= '<span class="highlighted-fact-text-' . strtolower($di['text_type']) . '">' . $di['text_text'] . "</span>";
        endforeach;
      endif;

      if ($data_item['caption'] !== ''):
        $html .= '<span class="highlighted-fact-caption">' . $data_item['caption'] . '</span>';
      endif;

      $html .= '</div>';

    endforeach;
    $html .= '</div></div>';

  endif;

  print $html;
?>
