/**
 * @file
 * Define tags as "block level" so the editor doesn't try to put paragraph tags around them.
 */

cktags = ['ckfactsfigures'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('embedded_factsfigures', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {
    // Define plugin name.
    var pluginName = 'embedded_factsfigures';

    // Set up object to share variables amongst scripts.
    CKEDITOR.embedded_factsfigures = {};

    // Register button for Facts/Figures.
    editor.ui.addButton('Facts and Figures', {
      label : "Add/Edit Facts and Figures",
      command : 'factsfigures',
      icon: this.path + 'icons/facts_and_figures.png',
    });
    // Register right-click menu item for Facts/Figures.
    editor.addMenuItems({
      factsfigures : {
        label : "Edit Fact and Figures",
        icon: this.path + 'icons/facts_and_figures.png',
        command : 'factsfigures',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckfactsfigures = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckfactsfigures = 1;
    CKEDITOR.dtd.body.ckfactsfigures = 1;
    // Make ckfactsfigures to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckfactsfigures = 1;

    // Make sure the fake element for Facts/Figures has a name.
    CKEDITOR.embedded_factsfigures.ckfactsfigures = 'factsfigures';
    CKEDITOR.lang.en.fakeobjects.ckfactsfigures = CKEDITOR.embedded_factsfigures.ckfactsfigures;
    // Add JavaScript file that defines the dialog box for Facts/Figures.
    CKEDITOR.dialog.add('factsfigures', this.path + 'dialogs/factsfigures.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('factsfigures', new CKEDITOR.dialogCommand('factsfigures'));

    // Regular expressions for Facts/Figures.
    CKEDITOR.embedded_factsfigures.factsfigures_regex = /^[1-9][0-9]*$/;

    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'ckfactsfigures') {
        evt.data.dialog = 'factsfigures';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'ckfactsfigures') {
          return { factsfigures : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(
      'img.ckfactsfigures {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/facts_and_figures2.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 200px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      // Facts/Figures: Definitions for wide width.
      '.uw_tf_standard_wide p img.ckfactsfigures {' +
        'height: 200px;' +
      '}' +

      // Facts/Figures: Definitions for standard width.
      '.uw_tf_standard p img.factsfigures {' +
        'height: 200px;' +
      '}'
    );
  },
  afterInit : function (editor) {
    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          ckfactsfigures : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckfactsfigures = CKEDITOR.embedded_factsfigures.ckfactsfigures;
            // Adjust title if a list name is present.
            if (element.attributes['factsfigures-nid']) {
              CKEDITOR.lang.en.fakeobjects.ckfactsfigures += ': ' + element.attributes['factsfigures-nid'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckfactsfigures', 'ckfactsfigures', false);
          }
        }
      });
    }
  }
});
