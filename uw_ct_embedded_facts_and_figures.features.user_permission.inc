<?php

/**
 * @file
 * uw_ct_embedded_facts_and_figures.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_embedded_facts_and_figures_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_embedded_facts_and_figures content'.
  $permissions['create uw_embedded_facts_and_figures content'] = array(
    'name' => 'create uw_embedded_facts_and_figures content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_embedded_facts_and_figures content'.
  $permissions['delete any uw_embedded_facts_and_figures content'] = array(
    'name' => 'delete any uw_embedded_facts_and_figures content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_embedded_facts_and_figures content'.
  $permissions['delete own uw_embedded_facts_and_figures content'] = array(
    'name' => 'delete own uw_embedded_facts_and_figures content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_embedded_facts_and_figures content'.
  $permissions['edit any uw_embedded_facts_and_figures content'] = array(
    'name' => 'edit any uw_embedded_facts_and_figures content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_embedded_facts_and_figures content'.
  $permissions['edit own uw_embedded_facts_and_figures content'] = array(
    'name' => 'edit own uw_embedded_facts_and_figures content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_embedded_facts_and_figures revision log entry'.
  $permissions['enter uw_embedded_facts_and_figures revision log entry'] = array(
    'name' => 'enter uw_embedded_facts_and_figures revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_facts_and_figures authored by option'.
  $permissions['override uw_embedded_facts_and_figures authored by option'] = array(
    'name' => 'override uw_embedded_facts_and_figures authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_facts_and_figures authored on option'.
  $permissions['override uw_embedded_facts_and_figures authored on option'] = array(
    'name' => 'override uw_embedded_facts_and_figures authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_facts_and_figures promote to front page option'.
  $permissions['override uw_embedded_facts_and_figures promote to front page option'] = array(
    'name' => 'override uw_embedded_facts_and_figures promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_facts_and_figures published option'.
  $permissions['override uw_embedded_facts_and_figures published option'] = array(
    'name' => 'override uw_embedded_facts_and_figures published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_facts_and_figures revision option'.
  $permissions['override uw_embedded_facts_and_figures revision option'] = array(
    'name' => 'override uw_embedded_facts_and_figures revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_facts_and_figures sticky option'.
  $permissions['override uw_embedded_facts_and_figures sticky option'] = array(
    'name' => 'override uw_embedded_facts_and_figures sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_embedded_facts_and_figures content'.
  $permissions['search uw_embedded_facts_and_figures content'] = array(
    'name' => 'search uw_embedded_facts_and_figures content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
