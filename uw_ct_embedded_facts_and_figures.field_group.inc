<?php

/**
 * @file
 * uw_ct_embedded_facts_and_figures.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_embedded_facts_and_figures_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eff_hf_fact_point|field_collection_item|field_eff_hf_fact_points|form';
  $field_group->group_name = 'group_eff_hf_fact_point';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_eff_hf_fact_points';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fact point',
    'weight' => '33',
    'children' => array(
      0 => 'field_eff_hf_caption',
      1 => 'field_eff_hf_icon',
      2 => 'group_eff_hf_fact_text',
      3 => 'group_infographic_settings',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-eff-hf-fact-point field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_eff_hf_fact_point|field_collection_item|field_eff_hf_fact_points|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eff_hf_fact_text|field_collection_item|field_eff_hf_fact_points|form';
  $field_group->group_name = 'group_eff_hf_fact_text';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_eff_hf_fact_points';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_eff_hf_fact_point';
  $field_group->data = array(
    'label' => 'Fact Text',
    'weight' => '46',
    'children' => array(
      0 => 'field_eff_hf_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-eff-hf-fact-text field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_eff_hf_fact_text|field_collection_item|field_eff_hf_fact_points|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_infographic_settings|field_collection_item|field_eff_hf_fact_points|form';
  $field_group->group_name = 'group_infographic_settings';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_eff_hf_fact_points';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_eff_hf_fact_point';
  $field_group->data = array(
    'label' => 'Infographic Settings',
    'weight' => '43',
    'children' => array(
      0 => 'field_eff_hf_fact',
      1 => 'field_eff_hf_type_of_graphic',
      2 => 'field_infographic_fact_suffix',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Infographic Settings',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'infographic-fields-inline',
        'description' => 'Displays an infographic based on a data point. Five options are available:
<ul><li>Number animation: Displays a rising number count based on a simple number (can include a decimal). </li>
    <li>Circle chart: Displays a rising circle chart in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage within the circle.</li>
    <li>Half circle chart: Displays a rising half circle chart in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage under the half circle.</li>
<li>Horizontal bar: Displays a rising horizontal bar in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage to the right of the horizontal bar.</li>
<li>Vertical bar: Displays a rising vertical bar in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage to the right of the vertical bar.</li>
</ul>
Infographics will appear in the middle of the fact bubble (below an icon and above fact text and captions).',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_infographic_settings|field_collection_item|field_eff_hf_fact_points|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Fact Text');
  t('Fact point');
  t('Infographic Settings');

  return $field_groups;
}
