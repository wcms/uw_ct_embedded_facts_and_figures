/**
 * @file
 */

(function ($) {

  Drupal.behaviors.uw_ct_embedded_facts_and_figures = {

    attach: function (context, settings) {

      $(window).ready(function () {

        // Check if we have FF in a promo item, if so process them.
        $('.node-uw-promotional-item div[class*="highlighted-fact-wrapper"]').each(function (index, value) {

          // If we have a carousel in the FF, add the Owl Carousel.
          if ($(this).attr("data-use-carousel") == "Yes") {

            // Add the Owl Carousel on the FF in the promo item.
            ($(this)).owlCarousel({
              items: 1,
              singleItem: true,
              navigation: true,
              navigationText: ["&lsaquo;&nbsp;prev", "next&nbsp;&rsaquo;"]
            });
          }
        });

        // Check if we have FF in a promo item, if so process them.
        $('.uw-site-sidebar--wrapper div[class*="highlighted-fact-wrapper"]').each(function (index, value) {

          // If we have a carousel in the FF, add the Owl Carousel.
          if ($(this).attr("data-use-carousel") == "Yes") {

            // Add the Owl Carousel on the FF in the sidebar.
            ($(this)).owlCarousel({
              items: 1,
              singleItem: true,
              navigation: true,
              navigationText: ["&lsaquo;&nbsp;prev", "next&nbsp;&rsaquo;"]
            });
          }
        });


        // Check if we have any other FF, if so process them.
        // Owl 1.x documentation: http://web.archive.org/web/20160306020452/http://www.owlgraphic.com/owlcarousel/
        $('div[class*="highlighted-fact-wrapper"]').each(function (index, value) {
          ($(this)).owlCarousel({
            items: $(this).attr("data-num-per"),
            itemsDesktop: [1024, $(this).attr("data-num-per")],
            itemsTablet: [768, 2],
            itemsMobile: [480, 1],
            navigation: true,
            addClassActive: true,
            navigationText: ["&lsaquo;&nbsp;prev", "next&nbsp;&rsaquo;"]
            // afterAction: runAnimation($(this).find('.active'))
          });
        });

        // For circle infographics, first check to make sure library is loaded
        if($.fn.circliful) {
          // Set default options for circle infographics
          $circliful_options = {
            animation: 1,
            animationStep: 3,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 15,
            backgroundColor: '#a2a2a2',
            animateInView: 'true',
            fontColor: '#4e4e4e',
            percentageTextSize: '35',
          };

          // Create circle infographic
          $('.infographic-circle').each(function() {
            $(this).circliful($circliful_options);
          });

          // Create half circle
          $('.infographic-half_circle').each(function() {
            $(this).circliful(
              $.extend($circliful_options, {halfCircle: 'true'})
            );
          });
        }

        $('.infographic-horizontal, .infographic-vertical', context).each(function() {
          $(this).append('<div class="graph-wrapper"><span class="graph"></span></div><span class="timer">' + $(this).data('percent') + '</span>');
        });

        // Check if the element is is in view
        function isElementInViewport(elem) {
          var $elem = $(elem);

          // Get the scroll position of the page.
          var viewportTop = $('html').scrollTop();
          var viewportBottom = viewportTop + $(window).height();

          // Get the position of the element on the page.
          // Adds a little padding so it triggers when the element is closer to the middle of the page
          var elemTop = Math.round( $elem.offset().top );
          var elemBottom = elemTop + $elem.height();
          return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
        }

        // Run the animation if the element is in view
        function checkAnimation() {
           $('.infographic-horizontal, .infographic-vertical, .infographic-number').each(function() {
            // Check to see if the animation has already been run
            if ($(this).hasClass('animated')) return;
            if (isElementInViewport($(this))) {
              runAnimation($(this));
            }
          });
        }

        // Check when the page loads
        checkAnimation();

        // Capture scroll events
        $(window).scroll(function(){
            checkAnimation();
        });

        // Isolating this to try to run it when the slider changes
        function runAnimation(el) {
            var type = el.data('infographic-type');
            var percent = el.data("percent");
            // Animate the graph
            if (type == 'horizontal') {
              el.find('.graph-wrapper .graph').animate({'width' : percent + '%'}, 2000);
            }
            else if (type == 'vertical') {
              el.find('.graph-wrapper .graph').animate({'height' : percent + '%'}, 2000);
            }
            animateNumber(el.find('.timer'), percent);
            el.addClass('animated');
        }

        // Animate the number.
        function animateNumber(el, percent) {
          // Find out if the number has a thousands marker (comma).
          if (percent.toString().indexOf(',') >= 0) {
            var has_comma = true;
            // If so, remove it for now so the animation will work.
            percent = percent.replace(/,/g, '');
          } else {
            has_comma = false;
          }

          $({counter: 0}).animate({counter: percent}, {
              duration: 2000,
              step: function() {
                var num = Math.ceil(this.counter).toString();
                if (has_comma) {
                  // Add back the comma.
                  while (/(\d+)(\d{3})/.test(num)) {
                    num = num.replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                  }
                }
                $(el).text(num);
              },
              // Ensure the correct value is shown when animation is complete
              // fixes issue with incomplete counters for large numbers
              // see: https://stackoverflow.com/questions/50331552/jquery-counter-fails-to-show-the-correct-value-after-animation
              complete : function(){
                if (has_comma) {
                  // Add back the comma.
                  percent = num.replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                }
                $(el).text(percent);
              }
          });

        }

     });
    }


  };
})(jQuery);
